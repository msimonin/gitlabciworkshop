# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

import sphinx_bootstrap_theme

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'projection'
copyright = '2023, Inria'
author = 'Vincent Rouvreau, Hande Gözükan'
release = '0.0.1'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    # extension to pull docstrings from modules to document
    'sphinx.ext.autodoc',
    # support for NumPy and Google style docstrings
    'sphinx.ext.napoleon',
    # to generate automatic links to the documentation of objects in other projects
    'sphinx.ext.intersphinx',
]

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']



# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'bootstrap'

html_theme_options = {
    'navbar_sidebarrel': False,
    'navbar_pagenav': False,
    'source_link_position': "",
    'navbar_links': [
        ("API", "api_reference"),
        ("GitLab",
         "https://gitlab.inria.fr/formations/integrationcontinue/gitlabciworkshop",
         True)
    ],
}

html_static_path = ['_static']

intersphinx_mapping = {
    'python': ('https://docs.python.org/3', None),
}
