import math
from random import randint
import numpy as np
import pytest
from projection import stereographic_projection

def normalise(line):
    """Returns sqrt(x² + y² + z² + ...)"""
    return math.sqrt(np.sum(line * line))

def test_infinite_alpha():
    # Generate 100 points in R^4 on a 3-sphere
    X = np.random.rand(100,4)
    pts = X / np.apply_along_axis(normalise, 1, X).reshape(X.shape[0], 1)
    
    # Project R^4 points in R^3
    out = stereographic_projection(pts)
    assert out.shape == (pts.shape[0], pts.shape[1] - 1)
    
    # This will assert a ZeroDivisionError
    pts[randint(0, 99)][3] = 1.
    with pytest.raises(ZeroDivisionError):
        out = stereographic_projection(pts)